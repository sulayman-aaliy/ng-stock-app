import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPopinComponent } from './user-popin.component';

describe('UserPopinComponent', () => {
  let component: UserPopinComponent;
  let fixture: ComponentFixture<UserPopinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserPopinComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPopinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
