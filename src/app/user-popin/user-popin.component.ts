import { AfterViewChecked, AfterViewInit, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { catchError } from 'rxjs/operators';
import { IBusinessError } from '../contracts/IBusinessError';
import { IUser } from '../contracts/IUser';
import { TempDataState } from '../contracts/TempDataState';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-popin',
  templateUrl: './user-popin.component.html',
  styleUrls: ['./user-popin.component.less']
})
export class UserPopinComponent implements OnInit, AfterViewInit, AfterViewChecked {
  @Input()
  user?: IUser;

  @Input()
  title?: string;

  @Output()
  saved = new EventEmitter<IUser>();

  @Output()
  closed = new EventEmitter();

  get firstName() {
    return this.userForm?.get("firstName");
  }
  get lastName() {
    return this.userForm?.get("lastName");
  }
  get age() {
    return this.userForm?.get("age");
  }
  get hasNoTitle() {
    return !this.title;
  }

  zIndex: number | "" = -2;
  isReadyToPop = false;
  userForm = new FormGroup({});
  constructor(private userService: UserService, private formBuilder: FormBuilder) {
    
  }

  ngOnInit(): void {
    this.userForm = this.formBuilder.group({
      // firstName: new FormControl(this.user?.firstName, [Validators.required]),
      firstName: new FormControl(this.user?.firstName/*, this.validateUser("firstName")*/),
      // lastName: new FormControl(this.user?.lastName, [Validators.required]),
      lastName: new FormControl(this.user?.lastName/*, this.validateUser("lastName")*/),
      // age: new FormControl(this.user?.age, [
      //   Validators.required,
      //   Validators.maxLength(4),
      //   Validators.min(0),
      //   Validators.max(1000),
      //   Validators.pattern("^\\d+$")
      // ])
      age: new FormControl(this.user?.age/*, this.validateUser("age")*/)
    });
  }

  ngAfterViewInit() {
    this.zIndex = "";
    this.isReadyToPop = true;
  }

  ngAfterViewChecked() {
    //console.log(this.userForm.controls["age"].errors);
  }

  validateUser(property: string): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (!this.user?._state) {
        return null;
      }
      if (!this.user._state.isValid(property)) {
        let x: { [key: string]: any } = { };
        x[this.user._state.value] = { value: control.value };
        return x;
      }
      return null;
    };
  }

  onSubmit(value: any) {
    if (value as IUser) {
      this.saveUser(value as IUser);
    }
  }

  saveUser(user: IUser) {
    delete this.user?._state;
    this.userService.addOrUpdate(user)
      .pipe(catchError((e, c) => {
        const errors = e as IBusinessError[];
        if (this.user && errors && errors.length > 0) {
          //this.user._state = new TempDataState(errors[0].targets, errors[0].type, 2);
          errors.forEach(x => {
            x.targets.forEach(y => {
              this.userForm.controls[y].setErrors({ [x.type]: true });
            });
          });
          this.userForm.markAllAsTouched();
        }
        throw e;
      }))
      .subscribe(u => {
        this.saved.emit(u);
        setTimeout(this.close.bind(this), 10);
      });
  }

  close() {
    this.closed.emit();
  }
}
