import { Component, Injector, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { createCustomElement, NgElement, WithProperties } from '@angular/elements';
import { ActivatedRoute } from '@angular/router';
import { PopinConfirmService } from '../popin-confirm.service';
import { PopinConfirmComponent } from '../popin-confirm/popin-confirm.component';
import { UserPopinComponent } from '../user-popin/user-popin.component';

@Component({
  selector: 'app-stock-root',
  templateUrl: './stock-root.component.html',
  styleUrls: ['./stock-root.component.less']
})
export class StockRootComponent implements OnInit, OnChanges {
  title = 'Stock';

  constructor(injector: Injector/*, public popinConfirm: PopinConfirmService, private route: ActivatedRoute*/) {
    // Convert `PopinConfirmComponent` to a custom element.
    const PopinConfirmElement = createCustomElement(PopinConfirmComponent, { injector });
    const UserPopinElement = createCustomElement(UserPopinComponent, { injector });
    // Register the custom element with the browser.
    customElements.define('popin-confirm-element', PopinConfirmElement);
    customElements.define('user-popin-element', UserPopinElement);
  }

  ngOnChanges(changes: SimpleChanges) {
    
  }

  ngOnInit() {
    // this.route.queryParams.subscribe(params => {
    //   this.name = params['name'];
    // });
  }

}
