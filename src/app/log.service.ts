import { HttpClient, HttpUrlEncodingCodec } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { ILog } from './contracts/ILog';
import { ISearchService } from './contracts/ISearchService';

@Injectable({
  providedIn: 'root'
})
export class LogService implements ISearchService<ILog> {

  latestSearch?: string;

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<ILog[]>("http://localhost:55000/logs");
  }

  search(text: string) {
    this.latestSearch = text;
    return this.http.get<ILog[]>("http://localhost:55000/logs/" + encodeURIComponent(text));
  }
  
}
