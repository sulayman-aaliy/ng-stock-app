import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { CellEditingStartedEvent, CellEditingStoppedEvent, CellValueChangedEvent, GridApi, GridOptions, GridReadyEvent } from 'ag-grid-community';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { TempDataState } from '../contracts/TempDataState';
import { IBusinessError } from '../contracts/IBusinessError';
import { IUser } from '../contracts/IUser';
import { UserService } from '../user.service';
import { ChatService } from '../chat.service';

@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.less']
})
export class ManageUsersComponent implements OnInit, AfterViewInit, OnDestroy {
  userGridOptions = {
    columnDefs: [
      { hide: true, colId: "uniqueId", field: "uniqueId", headerComponentParams: {} },
      { headerName: "First Name", field: "firstName" },
      { headerName: "Last Name", field: "lastName" },
      { headerName: "Age", field: "age", width: 120, resizable: false }
    ],
    defaultColDef: {
      editable: true,
      resizable: true,
      sortable: true,
      cellClassRules: { "invalid-ag-cell": "data._state && !data._state.isValid(colDef.field)" }
    },
    onCellValueChanged: this.saveUser,
    onCellEditingStarted: this.editUserStarted,
    onCellEditingStopped: this.editUserStopped,
    onGridReady: this.initGrid,
    animateRows: true,
    context: this
  }
  userList = new Observable<IUser[]>();
  constructor(private userService: UserService, private chatService: ChatService) {
    this.userGridOptions.columnDefs[this.userGridOptions.columnDefs.length - 1].headerComponentParams = {
      template: `<div class="ag-cell-label-container" role="presentation">
    <button id="btnNewUser" type="button">➕</button>
    <span ref="eMenu" class="ag-header-icon ag-header-cell-menu-button"></span>
    <div ref="eLabel" class="ag-header-cell-label" role="presentation">
        <span ref="eText" class="ag-header-cell-text" role="columnheader"></span>
        <span ref="eFilter" class="ag-header-icon ag-filter-icon"></span>
        <span ref="eSortOrder" class="ag-header-icon ag-sort-order"></span>
        <span ref="eSortAsc" class="ag-header-icon ag-sort-ascending-icon"></span>
        <span ref="eSortDesc" class="ag-header-icon ag-sort-descending-icon"></span>
        <span ref="eSortNone" class="ag-header-icon ag-sort-none-icon"></span>
    </div>
</div>`
    };
  }

  ngOnInit(): void {
    this.userList = this.userService.getAll();
  }
  initGrid(this: GridOptions, e: GridReadyEvent) {
    const states = this.columnApi?.getColumnState();
    const i = states?.findIndex(s => s.colId === "age");
    if (i !== undefined && states && i > -1) {
      states[i].sort = "desc";
      this.columnApi?.applyColumnState({ state: states });
    }
  }
  ngAfterViewInit() {
    document.querySelector("#btnNewUser")?.addEventListener("click", this.popinNewUser.bind(this));
  }
  ngOnDestroy() {
    document.querySelector("#btnNewUser")?.removeEventListener("click", this.popinNewUser.bind(this));
  }
  popinNewUser(e: Event) {
    const popin = this.userService.showDetails({ firstName: "", lastName: "", age: 0 });
    const c = this;
    popin.addEventListener("saved", e => c.userList = c.userService.getAll());
  }
  saveUser(this: GridOptions, e: CellValueChangedEvent) {
    console.log("CellValueChangedEvent raised");
    let user = e.data as IUser;
    delete user._state;
    const options = this;
    const component = options.context as ManageUsersComponent;
    component.userService.addOrUpdate(user)
      .pipe(catchError((err, c) => {
        const errors = err as IBusinessError[];
        if (errors && errors.length > 0) {
          e.node.setData({...user, _state: new TempDataState(errors[0].targets, errors[0].type, 1)});
        }
        setTimeout(() => {
          e.node.setDataValue(e.column, e.oldValue);
        }, 50);
        throw err;
      }))
      .subscribe(u => {
        console.log(u);
        const states = e.columnApi.getColumnState();
        if (states.some(s => s.sort)) {
          options.columnApi?.setColumnState(states);
        }
        //options.api?.refreshCells({ columns: [e.column] });
        //component.userList = component.userService.getAll();
      });
  }
  editUserStarted(this: GridOptions, e: CellEditingStartedEvent) {
    console.log("CellEditingStartedEvent raised");
  }
  editUserStopped(this: GridOptions, e: CellEditingStoppedEvent) {
    console.log("CellEditingStoppedEvent raised");
  }
}
