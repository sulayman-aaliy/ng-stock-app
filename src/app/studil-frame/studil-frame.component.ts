import { Component, Injector, OnInit } from '@angular/core';
import { createCustomElement, NgElement, WithProperties } from '@angular/elements';
import { PopinConfirmComponent } from '../popin-confirm/popin-confirm.component';
import { UserPopinComponent } from '../user-popin/user-popin.component';

declare global {
  interface HTMLElementTagNameMap {
    "popin-confirm-element": NgElement & WithProperties<PopinConfirmComponent>;
    "user-popin-element": NgElement & WithProperties<UserPopinComponent>;
  }
}

@Component({
  selector: 'app-studil-frame',
  templateUrl: './studil-frame.component.html',
  styleUrls: ['./studil-frame.component.less']
})
export class StudilFrameComponent implements OnInit {

  constructor(injector: Injector/*, public popinConfirm: PopinConfirmService, private route: ActivatedRoute*/) {
    // Convert `PopinConfirmComponent` to a custom element.
    const PopinConfirmElement = createCustomElement(PopinConfirmComponent, { injector });
    const UserPopinElement = createCustomElement(UserPopinComponent, { injector });
    // Register the custom element with the browser.
    customElements.define('popin-confirm-element', PopinConfirmElement);
    customElements.define('user-popin-element', UserPopinElement);
  }

  ngOnInit(): void {
  }

}
