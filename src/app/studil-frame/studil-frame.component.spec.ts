import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudilFrameComponent } from './studil-frame.component';

describe('StudilFrameComponent', () => {
  let component: StudilFrameComponent;
  let fixture: ComponentFixture<StudilFrameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudilFrameComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudilFrameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
