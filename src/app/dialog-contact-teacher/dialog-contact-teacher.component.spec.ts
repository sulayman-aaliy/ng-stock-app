import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogContactTeacherComponent } from './dialog-contact-teacher.component';

describe('DialogContactTeacherComponent', () => {
  let component: DialogContactTeacherComponent;
  let fixture: ComponentFixture<DialogContactTeacherComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogContactTeacherComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogContactTeacherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
