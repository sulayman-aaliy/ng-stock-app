import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-contact-teacher',
  templateUrl: './dialog-contact-teacher.component.html',
  styleUrls: ['./dialog-contact-teacher.component.less']
})
export class DialogContactTeacherComponent implements OnInit {
  helpRequest = { message: "", context: null };
  contactForm = this.formBuilder.group({
    message: new FormControl(this.helpRequest.message, [Validators.required, Validators.maxLength(500)])
  });

  constructor(private formBuilder: FormBuilder, public dialogRef: MatDialogRef<DialogContactTeacherComponent>) { }

  ngOnInit(): void {
  }

  onSubmit(request?: { message: string, context: any | null } | null) {
    if (request) {
      console.log(request);
      this.dialogRef.close();
    }
  }

}
