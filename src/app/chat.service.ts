import { Injectable } from '@angular/core';
import { io } from 'socket.io-client';

@Injectable({
  providedIn: "root"
})
export class ChatService {
  private readonly url = "http://localhost:55005";
  private socket;

  constructor() {
    this.socket = io(this.url);
  }
}
