import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-personal-infos',
  templateUrl: './personal-infos.component.html',
  styleUrls: ['./personal-infos.component.less']
})
export class PersonalInfosComponent implements OnInit {

  @Input()
  userProfile: any;

  languageList: string[];

  profileForm = new FormGroup({});
  constructor(private formBuilder: FormBuilder) {
    this.userProfile = { firstName: "", lastName: "", birthdate: null, languages: null, goal: "", email: "", phoneNumber: null, rootLanguage: null };
    this.languageList = ["Français", "Türk", "English", "Arabic"];
  }

  ngOnInit(): void {
    this.profileForm = this.formBuilder.group({
      firstName: new FormControl(this.userProfile?.firstName, [Validators.required, Validators.maxLength(120)]),
      lastName: new FormControl(this.userProfile?.lastName, [Validators.required, Validators.maxLength(120)]),
      birthdate: new FormControl(this.userProfile?.birthdate),
      languages: new FormControl(this.userProfile?.languages),
      rootLanguage: new FormControl(this.userProfile?.rootLanguage, [Validators.required]),
      goal: new FormControl(this.userProfile?.goal),
      email: new FormControl(this.userProfile?.email, [Validators.required, Validators.maxLength(240)]),
      phoneNumber: new FormControl(this.userProfile?.phoneNumber, [Validators.required, Validators.maxLength(20)]),
      phoneNumber2: new FormControl(this.userProfile?.phoneNumber2, [Validators.maxLength(20)])

      // age: new FormControl(this.user?.age, [
      //   Validators.required,
      //   Validators.maxLength(4),
      //   Validators.min(0),
      //   Validators.max(1000),
      //   Validators.pattern("^\\d+$")
      // ])
    });
  }

  onSubmit(profile: any) {
    if (profile) {
      console.log("ok");
    }
  }

  resetProfile() {
    console.log("reset");
  }

}
