import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StudentProfileRoutingModule } from './student-profile-routing.module';
import { ConnectionInfosComponent } from './connection-infos/connection-infos.component';
import { PersonalInfosComponent } from './personal-infos/personal-infos.component';
import { SubscriptionInfosComponent } from './subscription-infos/subscription-infos.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';


@NgModule({
  declarations: [
    ConnectionInfosComponent,
    PersonalInfosComponent,
    SubscriptionInfosComponent
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatNativeDateModule,
    MatMomentDateModule,
    MatDatepickerModule,
    MatListModule,
    MatSelectModule,
    MatButtonModule,
    MatIconModule,
    StudentProfileRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class StudentProfileModule { }
