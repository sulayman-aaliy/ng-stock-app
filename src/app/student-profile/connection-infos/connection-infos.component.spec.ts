import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectionInfosComponent } from './connection-infos.component';

describe('ConnectionInfosComponent', () => {
  let component: ConnectionInfosComponent;
  let fixture: ComponentFixture<ConnectionInfosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConnectionInfosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectionInfosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
