import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubscriptionInfosComponent } from './subscription-infos.component';

describe('SubscriptionInfosComponent', () => {
  let component: SubscriptionInfosComponent;
  let fixture: ComponentFixture<SubscriptionInfosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubscriptionInfosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscriptionInfosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
