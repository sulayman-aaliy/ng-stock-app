import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConnectionInfosComponent } from './connection-infos/connection-infos.component';
import { PersonalInfosComponent } from './personal-infos/personal-infos.component';
import { StudentProfileComponent } from './student-profile.component';
import { SubscriptionInfosComponent } from './subscription-infos/subscription-infos.component';

const routes: Routes = [
  {
    path: '', component: StudentProfileComponent, children: [
      { path: '', redirectTo: 'infos', pathMatch: 'full' },
      { path: 'infos', component: PersonalInfosComponent },
      { path: 'connection', component: ConnectionInfosComponent },
      { path: 'subscription', component: SubscriptionInfosComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentProfileRoutingModule { }
