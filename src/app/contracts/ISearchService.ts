import { Observable } from "rxjs";

export interface ISearchService<T> {
    latestSearch?: string;
    search: (text: string) => Observable<T[]>;
}