export interface ILog {
    user: string;
    message: string;
    event: string;
    type: string;
    status: string;
    dateOfEntry: Date;
}