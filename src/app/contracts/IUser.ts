import { TempDataState } from "./TempDataState";

export interface IUser {
    firstName: string;
    lastName: string;
    age: number;
    uniqueCode?: string;
    _state?: TempDataState | null;
}