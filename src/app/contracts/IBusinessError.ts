
export interface IBusinessError {
    message: string;
    type: string,
    targets: string[],
    code ?: number | null
}
