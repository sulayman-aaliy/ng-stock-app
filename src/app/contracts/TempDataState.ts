
export class TempDataState  {
    constructor(
        public properties: string[],
        public value: string,
        public duration: number = 0) {
    }

    public isValid(property?: string): boolean {
        const state = this;
        const invalid = !!state.value && (!property || this.properties.some(p => p === property));
        if (this.duration > 0) {
            setTimeout(state.reset, state.duration * 1000);
        }
        return !invalid;
    }

    public reset() {
        this.value = "";
        this.properties = [];
    }
}
