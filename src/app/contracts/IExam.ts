
export interface IExam {
    dateInit: Date;
    dateStart: Date | null;
    datesRestart: Date[] | null;
    dateClose: Date | null;
    studentId: number;
    icon: string;
    picture: string;
    title: string;
    subTitle: string;
    wording: string;
    questionList: IQuestion[];
    numberOfTries: number;
}

export interface IQuestion {
    id: number;
    text: string;
    picture: string | null;
    answerId: number;
    answerType: "text" | "single" | "multiple" | "audio";
    answerSelection: string[] | null;
    answers: IStudentAnswer[];
    newAnswer: string | null;
    isIgnored: boolean;
}

export interface IStudentAnswer {
    text: string | null;
    locked: boolean;
    date: Date | null;
}
