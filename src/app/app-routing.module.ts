import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LookupLogsComponent } from './lookup-logs/lookup-logs.component';
import { ManageUsersComponent } from './manage-users/manage-users.component';
import { StockComponent } from './stock/stock.component';
import { StudilHelpComponent } from './studil-help/studil-help.component';
import { UserListComponent } from './user-list/user-list.component';

const routes: Routes = [
  { path: "users", component: UserListComponent },
  { path: "stock", component: StockComponent },
  { path: "logs", component: LookupLogsComponent },
  { path: "user-list", component: ManageUsersComponent },
  { path: "profile", loadChildren: () => import(`./student-profile/student-profile.module`).then(m => m.StudentProfileModule) },
  { path: "help", component: StudilHelpComponent },
  { path: "dashboard", loadChildren: () => import(`./studil-dashboard/studil-dashboard.module`).then(m => m.StudilDashboardModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
