import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudilHelpComponent } from './studil-help.component';

describe('StudilHelpComponent', () => {
  let component: StudilHelpComponent;
  let fixture: ComponentFixture<StudilHelpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudilHelpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudilHelpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
