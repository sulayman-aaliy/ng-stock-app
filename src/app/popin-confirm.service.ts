import { Injectable } from '@angular/core';
import { NgElement, WithProperties } from '@angular/elements';
import { PopinConfirmComponent, PopinConfirmData } from './popin-confirm/popin-confirm.component';

@Injectable({ providedIn: 'root' })
export class PopinConfirmService {
    constructor() { }
    showMessageOnly(message: string) {
        let data: PopinConfirmData = { title: "ℹ Message", message: message, acceptText: "", declineText: "", ignoreText: "Close", closeOnClick: true, closeOnIgnore: false };
        this.show(data);
    }

    showYesNo(message: string) {
        let data: PopinConfirmData = { title: "❓ Confirm", message: message, acceptText: "Yes ✔", declineText: "No ❌", ignoreText: "", closeOnClick: true, closeOnIgnore: false };
        return this.show(data);
    }

    // This uses the new custom-element method to add the popup to the DOM.
    show(data?: PopinConfirmData | null) {
        if (!data) {
            data = { title: "❓", message: "Confirm?", acceptText: "✔", declineText: "❌", ignoreText: "Close", closeOnClick: true, closeOnIgnore: true };
        }

        // Create element
        const popinElement: NgElement & WithProperties<PopinConfirmComponent> = document.createElement('popin-confirm-element') as any;

        // Listen to the close event
        popinElement.addEventListener("closed", () => document.body.removeChild(popinElement));

        // Set the message
        this.setConfirmData(popinElement, data);

        // Add to the DOM
        document.body.appendChild(popinElement);

        return popinElement;
    }

    private setConfirmData(element: NgElement & WithProperties<PopinConfirmComponent>, data: PopinConfirmData) {
        element.message = data.message;
        element.title = data.title;
        element.buttonLabels = { accept: data.acceptText, decline: data.declineText, ignore: data.ignoreText };
    }
}