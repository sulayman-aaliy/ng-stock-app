import {
    Component,
    Input,
    OnInit,
    OnChanges,
    SimpleChanges,
    Output,
    EventEmitter
} from '@angular/core';
import { IUser } from '../contracts/IUser';

@Component({
    selector: 'app-user-edit',
    templateUrl: './user-edit.component.html',
    styleUrls: ['./user-edit.component.less'],
})
export class UserEditComponent implements OnInit, OnChanges {
    initialUser: IUser;

    @Input()
    user: IUser;
    
    @Input()
    isReadonly: boolean = true;

    @Input()
    justUpdatedUser: IUser | null = null;
    @Output()
    justUpdatedUserChange = new EventEmitter<IUser | null>();

    @Output()
    updateRequest = new EventEmitter<IUser>();
    @Output()
    deleteRequest = new EventEmitter<string>();
    @Output()
    cancelCreate = new EventEmitter();

    get isNew() {
        return this.user.uniqueCode === undefined;
    }

    constructor() {
        this.user = { firstName: "", lastName: "", age: 0 };
        this.initialUser = { firstName: "", lastName: "", age: 0 };
    }

    ngOnInit(): void {
        Object.assign(this.initialUser, this.user);
        if (this.isNew) {
            this.isReadonly = false;
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.justUpdatedUser.previousValue === null && changes.justUpdatedUser.currentValue) {
            let newUser: IUser = changes.justUpdatedUser.currentValue;
            if (newUser.uniqueCode && newUser.uniqueCode === this.user.uniqueCode) {
                setTimeout(() => {
                    Object.assign(this.initialUser, this.user);
                    //this.user = newUser;
                    this.isReadonly = true;
                    this.justUpdatedUser = null;
                }, 10);
            }
        }
    }

    initUpdate() {
        this.isReadonly = false;
        //this.justUpdatedUser = this.user;
    }

    cancelUpdate() {
        if (this.isNew) {
            this.cancelCreate.emit();
        }
        else {
            this.isReadonly = true;
            //this.justUpdatedUser = null;
            Object.assign(this.user, this.initialUser);
        }
    }

    claimUpdate() {
        this.updateRequest.emit(this.user);
    }

    claimDelete() {
        this.deleteRequest.emit(this.user.uniqueCode || "");
    }

    // typeFirstName(ev: Event) {
    //     this.user.firstName = (ev.target as HTMLInputElement).value;
    // }

    changeUser(ev: Event) {
        let i = (ev.target as HTMLInputElement);
        let a = i.name.split(".");
        a.shift();
        let u = this.user as any;
        u[a[0]] = i.value;
        this.user = u;
    }

    // typeAge(ev: Event) {
    //     this.user.age = parseInt((ev.target as HTMLInputElement).value);
    // }
}
