import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { IUser } from './contracts/IUser';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { NgElement, WithProperties } from '@angular/elements';
import { UserPopinComponent } from './user-popin/user-popin.component';
import { IBusinessError } from './contracts/IBusinessError';

@Injectable({ providedIn: 'root' })
export class UserService {
  constructor(private http: HttpClient) {

  }

  public showDetails(user: IUser) {
    const popinElement: NgElement & WithProperties<UserPopinComponent> = document.createElement('user-popin-element') as any;
    popinElement.addEventListener("closed", () => document.body.removeChild(popinElement));
    //popinElement.addEventListener("saved", () => document.body.removeChild(popinElement));
    popinElement.title = user.firstName ? "" : "➕ New User";
    popinElement.user = user;
    document.body.appendChild(popinElement);
    return popinElement;
  }

  getAll() {
    return this.http.get<IUser[]>("http://localhost:55000/users").pipe(catchError(this.handleError));
  }

  addOrUpdate(user: IUser) {
      if (user.uniqueCode) {
        return this.http.patch<IUser>(`http://localhost:55000/user/${user.uniqueCode}`, user).pipe(catchError(this.handleError));
      }
      else {
        return this.http.post<IUser>("http://localhost:55000/user", user).pipe(catchError(this.handleError));
      }
  }

  delete(code: string) {
    return this.http.delete<IUser>(`http://localhost:55000/user/${code}`).pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    }
    else if (error.error as IBusinessError[]) {
      const be = error.error as IBusinessError[];
      return throwError(be);
    }
    else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }
}