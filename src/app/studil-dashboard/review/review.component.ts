import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { IExam, IQuestion } from '../../contracts/IExam';
import { DialogContactTeacherComponent } from '../../dialog-contact-teacher/dialog-contact-teacher.component';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.less']
})
export class ReviewComponent implements OnInit {

  review: IExam;
  reviewForm = new FormGroup({});

  get hasIcon(): boolean {
    return this.review.icon !== "";
  }
  get hasSubTitle(): boolean {
    return this.review.subTitle !== "";
  }
  get hasPicture(): boolean {
    return this.review.picture !== "";
  }

  get pictureUrl(): string {
    return this.hasPicture ? `/assets/images/${this.review.picture}` : "";
  }
  get iconUrl(): string {
    return this.hasIcon ? `url('/assets/images/${this.review.icon}')` : "";
  }
  get allowStart(): boolean {
    return this.review.dateStart === null;
  }
  get allowRestart(): boolean {
    return this.review.dateStart !== null
      && this.isClosed
      && this.review.numberOfTries > 1
      && (this.review.datesRestart === null || (this.review.datesRestart.length + 1) <= this.review.numberOfTries);
  }
  get isStarted(): boolean {
    return !this.allowStart;
  }
  get isClosed() {
    return this.review.dateClose !== null;
  }

  constructor(private formBuilder: FormBuilder, public dialog: MatDialog) {
    this.review = {
      icon: "turkey-flag.png",
      picture: "salutations.png",
      dateInit: new Date(),
      dateStart: null,
      datesRestart: [],
      dateClose: null,
      title: "Traduction niveau 1",
      subTitle: "Les salutations",
      studentId: 0,
      questionList: [],
      wording: "Traduire les phrases",
      numberOfTries: 3
    };
  }

  ngOnInit(): void {


    if (!this.isClosed) {

      this.review.questionList = [
        { id: 1, answerId: 1, text: "Günaydın", picture: null, answerSelection: null, answerType: "text", answers: [], newAnswer: "", isIgnored: true },
        { id: 2, answerId: 2, text: "Bugün", picture: null, answerSelection: null, answerType: "text", answers: [], newAnswer: "auj", isIgnored: false },
        { id: 3, answerId: 3, text: "İyi akşamlar", picture: null, answerSelection: null, answerType: "text", answers: [], newAnswer: "", isIgnored: false }
      ];

      let array = new FormArray([]);
      this.review.questionList.forEach(q => {
        let c = new FormControl(q.newAnswer, Validators.required);
        if (q.isIgnored) {
          c.disable();
        }
        array.push(c);
      });
      this.reviewForm = this.formBuilder.group({ questions: array });
    }
  }

  showHelpPopin() {
    const dialogRef = this.dialog.open(DialogContactTeacherComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  startReview() {
    this.review.dateStart = new Date();
  }

  restartReview() {
    if (this.review.datesRestart?.length) {
      this.review.datesRestart.push(new Date());
    }
    else {
      this.review.datesRestart = [new Date()];
    }
    this.review.dateClose = null;
  }

  ignoreQuestion(q: IQuestion, isIgnored: boolean, index: number) {
    const a = this.reviewForm.get("questions") as FormArray;
    let control = a.at(index);
    q.isIgnored = isIgnored;
    if (isIgnored) {
      control.disable();
    }
    else {
      control.enable();
    }
  }

  onSubmit(list: string[]) {
    console.log(list);
    const a = this.reviewForm.get("questions") as FormArray;
    if (a.valid) {
      let index = 0;
      for (let i = 0; i < a.length; i++) {
        if (a.at(i).disabled) {
          continue;
        }
        this.review.questionList[i].newAnswer = list[index++];
      }
    }
  }

}
