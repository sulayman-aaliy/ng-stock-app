import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudilDashboardComponent } from './studil-dashboard.component';

describe('StudilDashboardComponent', () => {
  let component: StudilDashboardComponent;
  let fixture: ComponentFixture<StudilDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudilDashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudilDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
