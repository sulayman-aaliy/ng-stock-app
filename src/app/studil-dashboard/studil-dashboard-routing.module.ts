import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StudilDashboardComponent } from './studil-dashboard.component';
import { ReviewComponent } from './review/review.component';
import { ReviewListComponent } from './review-list/review-list.component';
import { StudentChatComponent } from './student-chat/student-chat.component';
import { LessonListComponent } from './lesson-list/lesson-list.component';

const routes: Routes = [
    {
        path: '', component: StudilDashboardComponent, children: [
            { path: '', redirectTo: 'latest-review', pathMatch: 'full' },
            { path: 'lesson-list', component: LessonListComponent },
            { path: 'latest-review', component: ReviewComponent },
            { path: 'review-list', component: ReviewListComponent },
            { path: 'chat', component: StudentChatComponent }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DashboardRoutingModule { }
