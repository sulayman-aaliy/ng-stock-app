import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReviewComponent } from './review/review.component';
import { ReviewListComponent } from './review-list/review-list.component';
import { StudentChatComponent } from './student-chat/student-chat.component';
import { DashboardRoutingModule } from './studil-dashboard-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatListModule } from '@angular/material/list';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatNativeDateModule } from '@angular/material/core';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StudentAnswersPipe } from '../student-answers.pipe';
import { MatInputModule } from '@angular/material/input';
import { LessonListComponent } from './lesson-list/lesson-list.component';
import { LessonComponent } from './lesson/lesson.component';

@NgModule({
  declarations: [
    ReviewComponent,
    ReviewListComponent,
    StudentChatComponent,
    StudentAnswersPipe,
    LessonListComponent,
    LessonComponent
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatMomentDateModule,
    MatDatepickerModule,
    MatListModule,
    MatSelectModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    MatTooltipModule,
    MatCardModule,
    MatCheckboxModule,
    MatMenuModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    DashboardRoutingModule
  ]
})
export class StudilDashboardModule {
  


}
