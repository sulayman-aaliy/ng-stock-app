import { Component, Input, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ISearchService } from '../contracts/ISearchService';

@Component({
  selector: 'app-debounce-search',
  templateUrl: './debounce-search.component.html',
  styleUrls: ['./debounce-search.component.less']
})
export class DebounceSearchComponent<T> implements OnInit {

  results$: Observable<T[]>;

  @Input()
  searchService: ISearchService<T>;

  private searchText$ = new Subject<string>();
  
  constructor() {
    this.results$ = new Observable<T[]>();
    this.searchService = { search: () => this.results$ };
  }

  search(e?: Event) {
    let text = (e?.target as HTMLInputElement)?.value || "";
    this.searchText$.next(text);
  }
  
  ngOnInit() {
    this.results$ = this.searchText$.pipe(
      debounceTime(500),
      distinctUntilChanged(),
      switchMap(text => this.searchService.search(text)));
      
    setTimeout(() => {
      if (this.searchService.latestSearch) {
        this.searchText$.next(this.searchService.latestSearch);
      }
      else {
        this.search();
      }
    }, 10);
  }

  refresh(text: string) {
    this.searchText$.next(text);
  }
}
