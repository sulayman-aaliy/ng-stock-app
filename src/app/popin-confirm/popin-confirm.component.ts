import { Component, Input, Output, OnInit, EventEmitter, AfterViewInit } from '@angular/core';
import { v4 as getUUIDv4 } from "uuid";

export interface PopinConfirmData {
  title: string;
  message: string;
  acceptText: string;
  declineText: string;
  ignoreText: string;
  closeOnClick: boolean;
  closeOnIgnore: boolean;
}

@Component({
  selector: 'app-popin-confirm',
  templateUrl: './popin-confirm.component.html',
  styleUrls: ['./popin-confirm.component.less']
})
export class PopinConfirmComponent implements OnInit, AfterViewInit {
  readonly popinRootClass = "popin-confirm";
  readonly prefixId = "_popin_";
  readonly rootId = `${this.prefixId}${getUUIDv4().replace(/-/ig, "")}`;

  isReadyToPop: boolean;
  popinCssLayout: any;

  get hasNoTitle() { return this.title.trim() === ""; };
  get hasDeclineAction() { return this.buttonLabels.decline.trim() !== "" };
  get hasIgnoreAction() { return this.buttonLabels.ignore.trim() !== "" };

  @Input()
  rootClasses: string[];

  @Input()
  title: string;

  @Input()
  message: string;

  @Input()
  buttonLabels: { accept: string, decline: string, ignore: string };
  
  @Input()
  closeOnIgnore: boolean = true;

  @Input()
  closeOnClick: boolean = true;

  @Output()
  confirm = new EventEmitter<boolean | null>();

  @Output()
  closed = new EventEmitter();

  constructor() {
    this.rootClasses = [this.popinRootClass];
    this.title = "Confirm";
    this.isReadyToPop = false;
    this.message = this.title;
    this.buttonLabels = { accept: "Yes", decline: "No", ignore: "Cancel" };
    this.popinCssLayout = { top: "0", left: "0", zIndex: "-2" };
  }

  ngOnInit(): void {
    if (!this.rootClasses.some(c => c === this.popinRootClass)) {
      this.rootClasses.unshift(this.popinRootClass);
    }
  }

  public setData(conf: PopinConfirmData) {
    this.message = conf.message;
    this.title = conf.title;
    this.buttonLabels = { accept: conf.acceptText, decline: conf.declineText, ignore: conf.ignoreText };
  }

  feedbackConfirm(ev: Event) {
    let b = ev.target as HTMLButtonElement;
    if (b.className === "ignore" && this.closeOnIgnore) {
      this.closed.emit();
      return;
    }
    this.confirm.emit(b.className === "accept" ? true : b.className === "decline" ? false : null);
    if (this.closeOnClick) {
      setTimeout(() => this.closed.emit(), 10);
    }
  }

  ngAfterViewInit() {
    if (!this.isReadyToPop) {
      let elements = [
        document.querySelector(`#${this.rootId} .title`),
        document.querySelector(`#${this.rootId} .message`),
        document.querySelector(`#${this.rootId} .popin-actions`)
      ].map(e => e as HTMLElement);
      let e = elements.sort((a, b) => a.offsetWidth < b.offsetWidth ? 1 : a.offsetWidth > b.offsetWidth ? -1 : 0)[0];
      let layout = { width: e.offsetWidth, height: elements.reduce((p, c) => p + c.offsetHeight, 0), left: 0, top: 0 };
      if (layout.width < document.body.offsetWidth * 0.2) {
        layout.width = document.body.offsetWidth * 0.2;
      }
      if (layout.height < document.body.offsetHeight * 0.2) {
        layout.height = document.body.offsetHeight * 0.2;
      }
      layout.left = (document.body.offsetWidth - layout.width) / 2;
      layout.top = (document.body.offsetHeight - layout.height) / 2;
      
      this.popinCssLayout = {
        top: `${layout.top}px`,
        left: `${layout.left}px`,
        width: `${layout.width}px`,
        height: `${layout.height}px`,
        zIndex: ""
      };

      this.isReadyToPop = true;
    }
  }
}
