import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PopinConfirmComponent } from './popin-confirm.component';

describe('PopinConfirmComponent', () => {
  let component: PopinConfirmComponent;
  let fixture: ComponentFixture<PopinConfirmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PopinConfirmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PopinConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
