import { Pipe, PipeTransform } from '@angular/core';
import { IStudentAnswer } from './contracts/IExam';

@Pipe({
  name: 'studentAnswers'
})
export class StudentAnswersPipe implements PipeTransform {

  transform(answers: IStudentAnswer[]): IStudentAnswer[] {
    let list = answers.filter(x => x.locked);
    list.sort((a, b) => a.date == b.date ? 0 : (a.date || new Date()) > (b.date || new Date()) ? 1 : -1);
    return list;
  }

}
