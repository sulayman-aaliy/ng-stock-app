import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StockPageComponent } from './stock-page/stock-page.component';

@NgModule({
  declarations: [StockPageComponent],
  imports: [
    CommonModule
  ]
})
export class StockModule { }
