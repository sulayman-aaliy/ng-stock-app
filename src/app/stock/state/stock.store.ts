import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { Stock } from './stock.model';

export interface StockState extends EntityState<Stock, string> {}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'stock' })
export class StockStore extends EntityStore<StockState> {

  constructor() {
    super();
  }

}
