import { Injectable } from '@angular/core';
import { NgEntityService } from '@datorama/akita-ng-entity-service';
import { StockStore, StockState } from './stock.store';

@Injectable({ providedIn: 'root' })
export class StockService extends NgEntityService<StockState> {

  constructor(protected store: StockStore) {
    super(store);
  }

}
