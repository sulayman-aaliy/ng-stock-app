import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import { UserService } from './user.service';
import { StockRootComponent } from './stock-root/stock-root.component';
import { PopinConfirmComponent } from './popin-confirm/popin-confirm.component';
import { PopinConfirmService } from './popin-confirm.service';
import { StockComponent } from './stock/stock.component';
import { LookupLogsComponent } from './lookup-logs/lookup-logs.component';
import { AgGridModule } from 'ag-grid-angular';
import { DebounceSearchComponent } from './debounce-search/debounce-search.component';
import { LogService } from './log.service';
import { AkitaNgDevtools } from '@datorama/akita-ngdevtools';
import { AkitaNgRouterStoreModule } from '@datorama/akita-ng-router-store';
import { environment } from '../environments/environment';
import { StockService } from './stock/state/stock.service';
import { NG_ENTITY_SERVICE_CONFIG } from '@datorama/akita-ng-entity-service';
import { ManageUsersComponent } from './manage-users/manage-users.component';
import { UserPopinComponent } from './user-popin/user-popin.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChatService } from './chat.service';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatListModule } from '@angular/material/list';
import { ErrorStateMatcher, MatNativeDateModule, ShowOnDirtyErrorStateMatcher } from '@angular/material/core';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatCardModule } from '@angular/material/card';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { DialogContactTeacherComponent } from './dialog-contact-teacher/dialog-contact-teacher.component';
import { StudentListComponent } from './student-list/student-list.component';
import { StudilFrameComponent } from './studil-frame/studil-frame.component';
import { StudilHelpComponent } from './studil-help/studil-help.component';
import { StudilDashboardComponent } from './studil-dashboard/studil-dashboard.component';
import { MatMenuModule } from '@angular/material/menu';
import { StudentProfileComponent } from './student-profile/student-profile.component';

@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    UserEditComponent,
    StockRootComponent,
    PopinConfirmComponent,
    StockComponent,
    LookupLogsComponent,
    DebounceSearchComponent,
    ManageUsersComponent,
    UserPopinComponent,
    UserProfileComponent,
    DialogContactTeacherComponent,
    StudentListComponent,
    StudilFrameComponent,
    StudilHelpComponent,
    StudilDashboardComponent,
    StudentProfileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AgGridModule.withComponents([]),
    environment.production ? [] : AkitaNgDevtools.forRoot(),
    AkitaNgRouterStoreModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatNativeDateModule,
    MatMomentDateModule,
    MatDatepickerModule,
    MatListModule,
    MatSelectModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    MatTooltipModule,
    MatCardModule,
    MatDialogModule,
    MatCheckboxModule,
    MatMenuModule
  ],
  providers: [
    UserService,
    LogService,
    PopinConfirmService,
    StockService,
    ChatService,
    { provide: NG_ENTITY_SERVICE_CONFIG, useValue: { baseUrl: 'https://jsonplaceholder.typicode.com' }},
    { provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher }
  ],
  bootstrap: [StudilFrameComponent]
})
export class AppModule { }
