import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { ILog } from '../contracts/ILog';
import { ISearchService } from '../contracts/ISearchService';
import { DebounceSearchComponent } from '../debounce-search/debounce-search.component';
import { LogService } from '../log.service';

@Component({
  selector: 'app-lookup-logs',
  templateUrl: './lookup-logs.component.html',
  styleUrls: ['./lookup-logs.component.less']
})
export class LookupLogsComponent implements OnInit, AfterViewInit {

  @ViewChild(DebounceSearchComponent)
  viewChild!: DebounceSearchComponent<ILog>;

  columnDefs = [
    { field: "type", filter: "agTextColumnFilter", width: 100 },
    { field: "event", resizable: true },
    { field: "message", resizable: true },
    { field: "dateOfEntry", headerName: "Date", sortable: true }
  ];

  get searchLogsService() {
    return this.logService as ISearchService<ILog>;
  }

  rowData = new Observable<ILog[]>();

  constructor(private logService: LogService) {
  }
  
  ngOnInit() {
  }

  ngAfterViewInit() {
    this.rowData = this.viewChild.results$;
  }
}
