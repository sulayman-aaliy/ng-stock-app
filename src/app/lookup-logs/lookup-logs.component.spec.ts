import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LookupLogsComponent } from './lookup-logs.component';

describe('LookupLogsComponent', () => {
  let component: LookupLogsComponent;
  let fixture: ComponentFixture<LookupLogsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LookupLogsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LookupLogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
