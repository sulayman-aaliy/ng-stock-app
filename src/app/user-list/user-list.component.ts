import { Component, OnInit, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';
import { IUser } from '../contracts/IUser';
import { PopinConfirmService } from '../popin-confirm.service';
import { UserService } from "../user.service";
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.less']
})
export class UserListComponent implements OnInit, OnChanges, OnDestroy {
  userList: IUser[];
  listSubscription: Subscription | null = null;
  actionSubscription: Subscription | null = null;
  updatedUser: IUser | null = null;

  get editingNewUser() {
    return this.userList.some(u => u.uniqueCode === undefined);
  }

  constructor(public service: UserService, public popinConfirm: PopinConfirmService, private route: ActivatedRoute) {
    this.userList = [];
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes);
  }

  ngOnInit(): void {
    this.route.url.subscribe(url => {
      let x = `/${url[0].path}`;
    });

    this.listSubscription = this.service.getAll().subscribe(list => {
      this.userList = list;
    });
    //catchError()
  }

  editNewUser() {
    let newList = [{ firstName: "", lastName: "", age: 0 }].concat(this.userList);
    this.userList = newList;
  }

  endEditNewUser() {
    this.reloadList();
  }

  updateUser(user: IUser) {
    this.updatedUser = null;
    if (this.actionSubscription !== null) {
      this.actionSubscription.unsubscribe();
    }
    this.actionSubscription = this.service.addOrUpdate(user).subscribe(u => {
      if (this.editingNewUser) {
        this.endEditNewUser();
      }
      else {
        this.updatedUser = u;
      }
    });
  }

  deleteUser(code: string) {
    this.popinConfirm.showYesNo("Confirm delete?").addEventListener("confirm", (x) => {
      //console.log(x);
      let ev = x as CustomEvent<boolean>;
      if (ev && ev.detail) {
        if (this.actionSubscription !== null) {
          this.actionSubscription.unsubscribe();
        }
        this.actionSubscription = this.service.delete(code).subscribe(x => this.reloadList());
      }
    });
    
  }

  reloadList() {
    if (this.listSubscription !== null) {
      this.listSubscription.unsubscribe();
    }
    this.listSubscription = this.service.getAll().subscribe(list => {
      this.userList = list;
    });
  }

  ngOnDestroy(): void {
    if (this.listSubscription !== null) {
      this.listSubscription.unsubscribe();
    }
    if (this.actionSubscription !== null) {
      this.actionSubscription.unsubscribe();
    }
  }
}
